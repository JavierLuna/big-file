## Big files test

## How to set up

Inside a virtual environment install dependencies found in `requirements.txt` like this:

``pip install -r requirements.txt``

You will also need the dataset.

You can extract `products.csv` in the project's root directory and run as is 
or configure its location inside `solution.py`

Run the solution like `python solution.py`. It'll generate a couple of CSVs which store the results.

## Comments

One of the bonus points was "as fast as response as possible", 
but I filled the script with `print` and `time.time()` calls to make the output as fancy as I could.

If going for true efficiency they could be removed and cut off some time.
