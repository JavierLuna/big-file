import pandas
import time

start_time = time.time()
INPUT_CSV = "products.csv"
TOP_N = 10
RED_REGEX = r"(\b|^[a-zA-Z0-9])red\b"

print("Loading {}...".format(INPUT_CSV), end="\t")
df = pandas.read_csv("products.csv")
print("OK")
finish_csv_read_time = time.time()

# Top N cheap and top N expensive
print("Finding out the top expensive and top cheap products...", end="\t")
df = df.sort_values('_current_price_value')
tn_cheap = df.head(TOP_N)
tn_expensive = df.tail(TOP_N)[::-1]
print("OK")

# Top N discount applied
print("Finding top discounts applied...", end="\t")
df['discount_applied'] = (df._original_price_value - df._current_price_value) / df._original_price_value
df = df.sort_values('discount_applied').drop("discount_applied", 1)
tn_discount = df.tail(TOP_N)
print("OK")

# Number of red products
print("Searching for red products...", end="\t")
red_only = df.color_name.str.contains(RED_REGEX, na=False, case=False)
df = df[red_only]
n_red = len(df)
print("OK")
print("Found {} red products!".format(n_red))

finish_querying_time = time.time()

print("Exporting top {} cheap...".format(TOP_N), end="\t")
tn_cheap.to_csv("top_{}_cheap.csv".format(TOP_N))
print("OK")

print("Exporting top {} expensive...".format(TOP_N), end="\t")
tn_expensive.to_csv("top_{}_expensive.csv".format(TOP_N))
print("OK")

print("Exporting top {} discount...".format(TOP_N), end="\t")
tn_discount.to_csv("top_{}_discount.csv".format(TOP_N))
print("OK")

finish_time = time.time()

print("\n\nDone, took {:.4f} seconds in total!\n\tReading csv = {:.4f}\n\tQuerying = {:.4f}\n\tWriting results = {:.4f}".format(
    finish_time - start_time, finish_csv_read_time - start_time, finish_querying_time - finish_csv_read_time,
    finish_time - finish_querying_time))
